import pygame
from tkinter import *
import sys
import random
from pygame.math import Vector2
from tkvideo import tkvideo
from PIL import ImageTk, Image
import moviepy.editor
from tkinter.font import Font
from moviepy.editor import *
from pygame.locals import (
    K_w,
    K_s
)


def startgame():
    class DOOMGUY():
        def __init__(self):
            self.body = [Vector2(7, 10), Vector2(6, 10), Vector2(5, 10)]
            self.direction = Vector2(1, 0)
            self.new_block = False
            self.shoot_sound = pygame.mixer.Sound('Music/Fire.mp3')
            self.death_sound = pygame.mixer.Sound('Music/Death.mp3')
            self.doomguy_head = pygame.image.load('Graphics/epic.png').convert_alpha()
            self.blood_head = pygame.image.load('Graphics/blood_01.png').convert_alpha()
            self.blood_head_02 = pygame.image.load('Graphics/blood_02.png').convert_alpha()
            self.blood_head_03 = pygame.image.load('Graphics/blood_03.png').convert_alpha()

        def draw_guy(self):
            # k=0
            for i, block in enumerate(self.body):
                block_x = int(block.x * cells_size)
                block_y = int(block.y * cells_size)
                self.block_rect = pygame.Rect(block_x, block_y, cells_size, cells_size)
                # k=k+1
                # print(k)
                if i == 0:
                    screen.blit(self.doomguy_head, self.block_rect)
                else:
                    if i > 15:
                        if i > 30:
                            screen.blit(self.blood_head_03, self.block_rect)
                        else:
                            screen.blit(self.blood_head_02, self.block_rect)
                    else:
                        screen.blit(self.blood_head, self.block_rect)

        def move_guy(self):
            if self.new_block == True:
                body_copy = self.body[:]
                body_copy.insert(0, body_copy[0] + self.direction)
                self.body = body_copy[:]
                self.new_block = False
            else:
                body_copy = self.body[:-1]
                body_copy.insert(0, body_copy[0] + self.direction)
                self.body = body_copy[:]

        def add_blood(self):
            self.new_block = True

        def play_shoot_sound(self):
            self.shoot_sound.play()

        def play_death_sound(self):
            self.death_sound.play()

    class DEMON():
        def __init__(self):
            self.random_demon()

        def draw_demon(self):
            demon_rect = pygame.Rect(int(self.pos.x * cells_size), int(self.pos.y * cells_size), cells_size, cells_size)
            screen.blit(monster, demon_rect)
            # pygame.draw.rect(screen, pygame.Color('green'), demon_rect)

        def random_demon(self):
            self.x = random.randint(1, cells_number - 2)
            self.y = random.randint(1, cells_number - 2)
            self.pos = pygame.math.Vector2(self.x, self.y)

    class MAIN():
        def __init__(self):
            self.doomguy = DOOMGUY()
            self.demon = DEMON()
            self.difficult = 100
            self.i = 1
            pygame.time.set_timer(SCREEN_UPDATE, self.difficult)

        def update(self):
            self.doomguy.move_guy()
            self.check_collision()
            self.check_fail()

        def draw_elements(self):
            self.demon.draw_demon()
            self.doomguy.draw_guy()
            self.score()

        def check_collision(self):
            if self.demon.pos == self.doomguy.body[0]:
                self.demon.random_demon()
                self.doomguy.add_blood()
                print('Snack')
                #blood_rect = pygame.Rect(int(self.demon.pos.x), int(self.demon.pos.y), cells_size, cells_size)
                #pygame.draw.rect(screen, pygame.Color('red'), blood_rect)
                # screen.blit(monster, (self.demon.pos.x, self.demon.pos.y))
                # kishki = monster.get_rect(center=(self.demon.pos.x, self.demon.pos.y))
                # screen.blit(monster, kishki)

                self.difficult = self.difficult - 1
                self.i = self.i + 1
                print('Жажда крови: ', self.i)
                self.doomguy.play_shoot_sound()

            if self.i > 4:
                if self.i > 15:
                    if self.i > 30:
                        if self.i > 50:
                            if self.i > 70:
                                self.doomguy.doomguy_head = pygame.image.load('Graphics/doom_05.png').convert_alpha()
                            else:
                                self.doomguy.doomguy_head = pygame.image.load('Graphics/doom_04.png').convert_alpha()
                        else:
                            self.doomguy.doomguy_head = pygame.image.load('Graphics/doom_03.png').convert_alpha()
                    else:
                        self.doomguy.doomguy_head = pygame.image.load('Graphics/doom_02.png').convert_alpha()
                else:
                    self.doomguy.doomguy_head = pygame.image.load('Graphics/doom_01.png').convert_alpha()

        def check_fail(self):
            if (self.doomguy.body[0].x < 0) or (self.doomguy.body[0].x >= cells_number) or (
                    self.doomguy.body[0].y < 0) or \
                    (self.doomguy.body[0].y >= cells_number):
                self.doomguy.play_death_sound()
                self.game_over()

            for block in self.doomguy.body[1:]:
                if block == self.doomguy.body[0]:
                    self.doomguy.play_death_sound()
                    self.game_over()

        def game_over(self):
            pygame.mixer.music.stop()
            run = False
            root = Tk()

            def game_exit():
                root.destroy()
                pygame.quit()
                sys.exit()

            root.geometry('500x500')
            root.resizable(width=False, height=False)
            root.configure(background='white')

            canva = Canvas(root, height=500, width=500,  bg='white',  relief="flat")
            canva.pack()

            title = Label(canva, text='Вы погибли', font='Corbel 25', bg='white',  relief="flat")
            title.pack(padx=10)

            btn_01 = Button(canva, text='Выйти', font='Corbel 20', bg='white', command=game_exit, relief="flat")
            btn_01.pack()

            root.mainloop()

        def score(self):
            text = str(len(self.doomguy.body) - 3)
            surface = font.render(text, True, (56, 74, 12))
            score_pos_x = int(cells_size * cells_number - 60)
            score_pos_y = int(cells_size * cells_number - 60)
            rect = surface.get_rect(center=(score_pos_x, score_pos_y))
            score_mob_rect = monster.get_rect(midright=(rect.left - 10, rect.centery))
            back_score_rect = pygame.Rect(score_mob_rect.left, score_mob_rect.top, score_mob_rect.height + 50,
                                          score_mob_rect.width)

            f1 = pygame.font.Font(None, 36)
            text1 = f1.render('Жажда крови ' + str(self.i), True, (255, 255, 255))
            screen.blit(text1, (460, 10))

            f2 = pygame.font.Font(None, 36)
            text1 = f1.render('Вы расчленили: ', True, (255, 255, 255))
            screen.blit(text1, (650, 925))

            pygame.draw.rect(screen, ('red'), back_score_rect)
            screen.blit(monster, score_mob_rect)
            screen.blit(surface, rect)

    pygame.init()

    pygame.mixer.music.load('Music/Hell.mp3')
    pygame.mixer.music.play(-1)

    cells_size = 50
    cells_number = 20

    screen = pygame.display.set_mode((cells_number * cells_size, cells_number * cells_size))
    clock = pygame.time.Clock()

    monster = pygame.image.load('Graphics/monster.png').convert_alpha()
    background = pygame.image.load('Graphics/wall.png').convert_alpha()
    font = pygame.font.Font(None, 25)

    SCREEN_UPDATE = pygame.USEREVENT

    main_game = MAIN()
    vol = 1.0
    while True:
        screen.fill((0, 0, 0))
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == SCREEN_UPDATE:
                main_game.update()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    if main_game.doomguy.direction.y != 1:
                        main_game.doomguy.direction = Vector2(0, -1)
                if event.key == pygame.K_DOWN:
                    if main_game.doomguy.direction.y != -1:
                        main_game.doomguy.direction = Vector2(0, 1)
                if event.key == pygame.K_RIGHT:
                    if main_game.doomguy.direction.x != -1:
                        main_game.doomguy.direction = Vector2(1, 0)
                if event.key == pygame.K_LEFT:
                    if main_game.doomguy.direction.x != 1:
                        main_game.doomguy.direction = Vector2(-1, 0)
                if event.key == pygame.K_s:
                    vol = vol - 0.1
                    pygame.mixer.music.set_volume(vol)
                    print('Громкость понижена: ', pygame.mixer.music.get_volume())
                if event.key == pygame.K_w:
                    vol = vol + 0.1
                    pygame.mixer.music.set_volume(vol)
                    print('Громкость повышена: ', pygame.mixer.music.get_volume())

        main_game.draw_elements()
        pygame.display.update()
        clock.tick(100)


def menu():
    # ---------------------- Funcs ------------------

    def starting():
        startgame()

    def game_over():
        screen.destroy()
        sys.exit()

    def new_window():

        def inmain():
            window.destroy()

        window = Tk()
        window.geometry('1600x900')
        window.resizable(width=False, height=False)
        window.configure(background='white')

        canva_01 = Canvas(window, height=1200, width=900, bg='white')
        canva_01.pack()

        pygame.mixer.init()

        pygame.mixer.music.load('Music/Help_song.mp3')
        pygame.mixer.music.play(-1)

        title_01 = Label(canva_01, text='Вы пробудились после долгого сна, время начать резню.'
                                        'Стрелочки - управление, w и s - регулировка звука. Чем больше '
                                        'жажда крови - тем сложнее выжить.', font='Corbel 16', bg='white')
        title_01.pack()

        title_02 = Label(canva_01, text='С каждым убитым монстром за вами обязательно придут, не дайте себя поймать', font='Corbel 16', bg='white')
        title_02.pack()

        title_03 = Label(canva_01, text='Выхода нет',
                         font='Corbel 16', bg='white')
        title_03.pack()

        btn_04 = Button(canva_01, text='Назад', font='Corbel 14', command=inmain, bg='white', relief="flat")
        btn_04.pack(padx=10)

        window.mainloop()

    # ---------------------- Funcs ------------------

    screen = Tk()
    screen.geometry('1920x1080')
    screen.resizable(width=False, height=False)
    screen.configure(background='white')

    canva = Canvas(screen, height=1920, width=1080, bg='white')
    canva.pack()

    # ---------------------- Labels ------------------

    vid_label = Label(screen, relief="flat")
    vid_label.pack()

    img = ImageTk.PhotoImage(Image.open("Graphics/font.jpg"))
    img_label = Label(canva, image=img, relief="flat")
    img_label.pack()

    img_02 = ImageTk.PhotoImage(Image.open("Graphics/slayer.png"))
    canva.create_image(70, 240, anchor=NW, image=img_02)

    img_03 = ImageTk.PhotoImage(Image.open("Graphics/slayer_01.png"))
    canva.create_image(700, 240, anchor=NW, image=img_03)

    # ---------------------- Labels ------------------

    player = tkvideo("Music/DOOM.mp4", vid_label, loop=1, size=(1070, 700))
    player.play()

    pygame.mixer.init()
    pygame.mixer.music.load('Music/Menu_song_01.mp3')
    pygame.mixer.music.play(-1)

    btn_01 = Button(canva, text='Начать игру', font='Corbel 20', command=starting, bg='white', relief="flat")
    btn_01.pack(pady=5)

    btn_02 = Button(canva, text='Выход', font='Corbel 20', command=game_over, bg='white', relief="flat")
    btn_02.pack(pady=5)

    btn_03 = Button(canva, text='Помощь', font='Corbel 20', command=new_window, bg='white', relief="flat")
    btn_03.pack(pady=5)

    screen.mainloop()


menu()
