# О предмете (RU)

Программирование основывается на использовании языков программирования и средств программирования. В основном языки программирования основаны на текстовом представлении программ, но иногда программировать можно используя, например, визуальное программирование или "zero-code" программирование.

# About subject (ENG)

Programming is based on the use of programming languages and programming tools. Programming languages are mostly based on the textual representation of programs, but sometimes you can program using, for example, visual programming or "zero-code" programming.


## Демонстрация работы по заданиям

    1) Brutal Snake

    Написать игру "Змейка" с помощью библиотеки PyGame. 
    Поле должно быть не меньше 100х100. 
    Змейка должна кушать яблочки со звуком и отращивать хвост. 
    При столкновении со стеной должен выдаваться звуковой сигнал и сообщение 
    (с помощью библиотеки Tk) об окончании игры. 
    Во время игры должна быть ненавязчивая фоновая музыка. 
    Правила игры оформить в виде справки в меню в верхней части окна 
    (как вариант можно оформить красивое игровое меню средствами PyGame).

![1](brutal_snake/snake.gif "brutal_snake")

    2) Sprites Game

    Создать простую игру на основе спрайтов, используя
    библотеку PyGame
    
![2](sprites_game/sprites.gif "sprites_game")
