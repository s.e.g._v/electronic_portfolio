button = document.querySelector("a.nav__login")
dialogMenu = document.querySelector("div.dialog_menu")
bgDialog = document.querySelector("div.dialog_menu > div:first-child")
dialog = document.querySelector("div.dialog_menu > div:last-child")

button.addEventListener("click", () => {
    dialogMenu.style.zIndex = 999999
    bgDialog.style.opacity = 0.5
    dialog.style.top = 0;
})

closebtn = document.querySelectorAll("div.dialog > div, div.dialog_menu > div:first-child")
closebtn.forEach(element => {
    element.addEventListener("click", () => {
        setTimeout(() => {dialogMenu.style.zIndex = -999999}, 500)
        bgDialog.style.opacity = 0
        dialog.style.top = "-1000px"
    })
});