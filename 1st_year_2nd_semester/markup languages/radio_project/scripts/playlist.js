const screenWidth = window.innerWidth

if (screenWidth < 480) exit()

window.addEventListener(`resize`, event => {
    const screenWidth = window.innerWidth
    if (screenWidth < 480) exit()
  });

playlists = document.querySelectorAll(".playlists > div > div:not(div:last-child)")
playlists.forEach(element => {
    element.addEventListener("click", event => showlist(event))
});

show = false
let lastlist = 0

headers = ["For Travel", "Work Out", "Relax", "Chart"]
musiclist = [[["Ysa Ferrer - Je vois", "02:58"], ["Би-2 - Молитва", "05:37"], ["Три дня дождя - Отпускай", "03:27"]],
[["Arash - Dooset Daram", "03:17"], ["Cyan Kicks - Let Me Down Slowly", "03:11"], ["Los Tiburones - Quizás", "03:09"]], 
[["Alis Shuka - Not About Us", "02:09"], ["Три дня дождя - Красота", "02:57"]], 
[["Arash Broken - Angel", "03:13"], ["INNA - UP", "02:30"], ["Lil Peep - Falling Down", "03:16"]]]


function showlist(e) { 
    console.log(e.path)
    if (e.target.tagName == "IMG") parent = e.path[2]
    else parent = e.path[1]
    for (let i = 0; i < e.path.length; i++) {
        if (e.path[i].tagName === "FIGURE") {
            parent = e.path[i + 1]
            break
        }
    }
    let k = 1
    child = document.querySelector(".playlists > div").childNodes
    for (i = 1; i < child.length; i += 2) {
        if (child[i] == parent) break
        else k++
    }
    if (!show) {
        document.querySelector(".list").style.display = "block"
        document.querySelector(".playlist" + k).style.display = "block"
        show = true
        lastlist = k
    } else {
        console.log(lastlist)
        document.querySelector(".playlist" + lastlist).style.display = "none"
        console.log(k)
        document.querySelector(".playlist" + k).style.display = "block"
        lastlist = k
    }
}