let lastmusic = 0
lastmusic = document.querySelectorAll("div.lastmusic > .player .play")
let playerbtn = document.querySelector(".playmusic > .play")
let music
let holding = false

const input = document.querySelectorAll('input[type="range"]')
input.forEach((item) => item.addEventListener('input', handleInputChange))
function handleInputChange(event) {
    let target = event.target
    const val = target.value
    target.style.backgroundSize = val + '% 100%'
}
document.querySelector(".time > input").addEventListener("mousedown", () => {holding = true})
document.querySelector(".time > input").addEventListener("mouseup", () => {holding = false})


    let audio = new Audio()
    let target
    lastmusic.forEach((item) => item.addEventListener('click', click))

    function click(event) {
        if (target != undefined && event.target != target) {
            target.style.backgroundPosition = "-5px 0px"
            audio.pause()
            audio = new Audio()
            music = undefined
        }
        target = event.target
        readymusic(target)
    }
    
    function readymusic(target) {
        let children = target.closest(".player").childNodes
        let path = getPath(children[3].textContent)
        let button = document.querySelector(".playmusic > .play")
        if (music === undefined) audio = playmusic(path, children[3].textContent, target, button)
        else if (music) pausemusic(target, button);
        else continuemusic(target, button)
        music = !music
    }
    
    function getPath(title) {
        let name = document.getElementsByTagName("title")[0].innerHTML;
        if (name == "Главная")
            return "music/" + title.replace("- ", "") + ".mp3"
        return "../music/" + title.replace("- ", "") + ".mp3"
    }
    
    let volume = document.querySelector(".range > input")
    volume.addEventListener("click", setVolume)
    function setVolume() {
        audio.volume = volume.value / 100
    }
    
    let player = document.querySelector(".playmusic")
    let time = document.querySelector(".time > input")
    let timer = document.querySelector(".playmusic > .timer")
    function playmusic(path, title, target, button) {
        button.style.backgroundPosition = "-79px -2px"
        target.style.backgroundPosition = "-33px 0"
        player.style.display = "flex"
        time.closest("div").style.display = "block"
        audio.src = path
        audio.currentTime = 0
        audio.play()
        audioPlay = setInterval(() => {
            let audioLength = Math.round(audio.duration)
            time.addEventListener("click", setTime)
            let audioTime = Math.round(audio.currentTime)
            let width = (audioTime * 100) / audioLength
            !holding ? time.style.backgroundSize = width + "% 100%" : ""
            timer.textContent = parseInt(audioTime / 60) + ":" + parseInt((audioTime % 60) / 10 + "") + (audioTime % 60) % 10
            audio.addEventListener("ended", switchaudio)
        }, 10)
        document.querySelector(".playmusic > .title").textContent = title
        return audio;
    }
    
    function setTime() {
        audio.currentTime = audio.duration * time.value / 100
        console.log(time.value)
    }
    let button = document.querySelector(".playmusic > .play")
    button.addEventListener("click", function() {
        music ? pausemusic(target, this) : continuemusic(target, this)
        music = !music
    })
    function pausemusic(target, button) {
        button.style.backgroundPosition = "-47px -2px"
        target.style.backgroundPosition = "-5px 0px"
        audio.pause()
    }
    
    function continuemusic(target, button) {
        button.style.backgroundPosition = "-79px -2px"
        target.style.backgroundPosition = "-33px 0"
        audio.play()
    }
    
    let nextbtn = document.querySelector(".playmusic > .next")
    let prevbtn = document.querySelector(".playmusic > .prev")
    nextbtn.addEventListener("click", switchaudio)
    prevbtn.addEventListener("click", switchaudio)
    let playlist = []
    function findtrack() {
        let children = document.querySelector("div.lastmusic").childNodes
        console.log(children)
        let k = 0
        let current = 0
        children.forEach((child) => {
            if (child.className === "player") {
                playlist.push(child.childNodes[1])
                k++
            }
            if (child.className === "player" && child.childNodes[3].textContent == document.querySelector(".title").textContent) {
                current = k
            }
        })
        return current;
    }

    function switchaudio(eventbtn) {
        let k = findtrack()
        if (eventbtn.target.className == "prev") {
            k - 1 > 0 ? k -= 2 : k = playlist.length - 1
        }
        audio.pause()
        audio = new Audio()
        target.style.backgroundPosition = "-5px 0px"
        if (k < playlist.length) newmusic = playlist[k]
        else newmusic = playlist[0]
        newmusic.addEventListener("click", click)
        let event = new MouseEvent("click")
        newmusic.dispatchEvent(event, {bubbles: true})
    }