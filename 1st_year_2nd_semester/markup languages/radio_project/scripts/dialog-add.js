function readURL(input) {
    console.log(input.files)
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
            document.querySelector("div.dialog > div:nth-of-type(2) img").setAttribute("src", e.target.result);
            document.querySelector("div.dialog > div:nth-of-type(2) img").style.display = "block"
            document.querySelector("div.dialog > div:nth-of-type(2) > img:last-child").style.display = "block"
        }
        reader.readAsDataURL(input.files[0]);
    }
}

document.querySelector("#file").addEventListener("change", function() {
    readURL(this);
});