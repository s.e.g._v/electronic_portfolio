let argument = document.forms[0].elements.number
console.log(argument)

const FormNumber = myForm.number
console.log(FormNumber)
const FormFullname = myForm.fullname
console.log(FormFullname)


window.handleForm=function(form) {

  const data={

        companyname: form.companyname.value,
        fullname: form.fullname.value,
        status: form.status.value,
        number: form.number.value,
        mail: form.mail.value,
        adress: form.adress.value,
          
    }

    /*-------------- Скелет визитной карточки --------------------*/

  let vizitka = document.createElement('div');
  vizitka.style.width = 445 + 'px';
  vizitka.style.height = 268 + 'px';
  vizitka.style.backgroundColor  = 'white';
  vizitka.style.position = 'absolute';
  vizitka.style.top = '10%';
  vizitka.style.right = '5%';
  vizitka.style.boxShadow = '0px 0px 6px #00000029';

  document.body.append(vizitka)

  /*-------------- Скелет центрального блока --------------------*/

  let center_text = document.createElement('div');
  center_text.style.textAlign = 'center';
  center_text.style.marginTop = '5%';

  vizitka.append(center_text)

  /*-------------- Скелет правого блока --------------------*/

  let right_text = document.createElement('div');
  right_text.style.textAlign = 'left';
  right_text.style.marginTop = '10%';
  right_text.style.marginLeft = '55%';
  right_text.style.font = 'normal normal normal 12px/14px Roboto';
  right_text.style.letterSpacing = 0 + 'px';
  right_text.style.color = '#202020';
  right_text.style.opacity = '1';

  vizitka.append(right_text)

  /*-------------- Имя организации --------------------*/

  let companyName = document.createElement('p');
  companyName.append(data.companyname);
  companyName.style.font = 'normal normal normal 10px/13px Roboto';
  companyName.style.letterSpacing = '0px';
  companyName.style.color = '#202020';
  center_text.append(companyName)

  /*-------------- ФИО --------------------*/

  let fullName = document.createElement('p');
  fullName.append(data.fullname);
  fullName.style.font = 'normal normal bold 26px/32px Roboto';
  fullName.style.letterSpacing = '0px';
  fullName.style.color = '#202020';
  fullName.style.fontWeight = 'bold';
  center_text.append(fullName)

  /*-------------- Должность --------------------*/

  let statusName = document.createElement('p');
  statusName.append(data.status);
  statusName.style.font = 'normal normal normal 14px/17px Roboto';
  statusName.style.letterSpacing = '0px';
  statusName.style.color = '#202020BF';
  center_text.append(statusName)

  /*-------------- Номер --------------------*/

  let numberName = document.createElement('p');
  numberName.append(data.number);
  numberName.style.font = 'normal normal normal 12px/14px Roboto';
  numberName.style.letterSpacing = '0px';
  numberName.style.color = '#202020';
  right_text.append(numberName)

  /*-------------- Email --------------------*/

  let numberEmail = document.createElement('p');
  numberEmail.append(data.mail);
  numberEmail.style.font = 'normal normal normal 12px/14px Roboto';
  numberEmail.style.letterSpacing = '0px';
  numberEmail.style.color = '#202020';
  right_text.append(numberEmail)

  /*-------------- Адрес --------------------*/

  let adressName = document.createElement('p');
  adressName.append(data.adress);
  adressName.style.font = 'normal normal normal 12px/14px Roboto';
  adressName.style.letterSpacing = '0px';
  adressName.style.color = '#202020';
  right_text.append(adressName)



  return false
}
