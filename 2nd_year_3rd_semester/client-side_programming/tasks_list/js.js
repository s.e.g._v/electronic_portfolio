                let todoList = document.querySelector('.todo-list')
                let addButton = todoList.querySelector('.task-list__add-button')

                let taskList = todoList.querySelector('.task-list')
                let taskForm = todoList.querySelector('.task-form')
                let form = taskForm.querySelector('form')
                let closeButton = taskForm.querySelector('.task-form__close')

                let formAction = ''
                let formItem;

                let editAction = e => {
                    let btn = e.target
                    let item = btn.closest('.task-list__item')
                    let prio = item.querySelector('.task-list__prio')
                    let desc = item.querySelector('.task-list__description')
                    form.reset();
                    form.description.value = desc.textContent
                    console.log(prio.innerHTML !== '','check')
                    form.prio.checked = (prio.innerHTML !== '')
                    taskForm.classList.add('shown')
                    formAction = 'edit'
                    formItem = item
                }
                let delAction = e => {
                    let btn = e.target
                    let item = btn.closest('.task-list__item')
                    item.remove()
                    if (taskForm.classList.contains('shown')){
                        taskForm.classList.remove('shown')
                    }
                    
                }
                form.addEventListener('submit', e =>{
                    e.preventDefault();
                    switch (formAction) {
                        case 'create':
                            let item = taskList.firstElementChild.cloneNode(true)
                            item.classList.toggle('zero_item')
                            item.querySelector('.task-list__done').checked = false
                            item.querySelector('.task-list__description').textContent = form.description.value
                            console.log(item.querySelector('.task-list__description').textContent)
                            item.querySelector('.task-list__prio').innerHTML = form.prio.checked ? '<img src="images/hiprio.svg" alt="приоритет">' : ''
                            taskList.append(item)
                            item.querySelector('.task-list__edit-button').addEventListener('click',editAction)
                            item.querySelector('.task-list__del-button').addEventListener('click',delAction)
                            taskForm.classList.toggle('shown')
                            form.reset()
                            break
                        case 'edit':
                            formItem.querySelector('.task-list__description').textContent = form.description.value
                            formItem.querySelector('.task-list__prio').innerHTML = form.prio.checked ? '<img src="images/hiprio.svg" alt="приоритет">' : ''
                            taskForm.classList.toggle('shown')
                            break
                    }
                })
                addButton.addEventListener('click', e =>{
                    taskForm.classList.toggle('shown')
                    formAction = 'create'
                    form.reset()
                    formItem = null
                })
                closeButton.addEventListener('click', e =>{
                    taskForm.classList.remove('shown')
                })
            