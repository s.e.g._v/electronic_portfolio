
//  3.2 Создать web-страницу, в которой отображается таблица умножения для чисел от 1 до 10. //

let table = document.createElement("table");

document.body.append(table);

for (let i = 1; i <= 10; i++) {
  let tr = document.createElement("tr");

  table.appendChild(tr);
  
  for (let j = 1; j <= 10; j++) {
    let td = document.createElement("td");
    
    tr.appendChild(td);
    td.innerText = i * j;
  }
}

//  3.1. Создать web-страницу, в которой отображается календарь на текущий месяц в виде таблицы. //


let div = document.createElement("div");
document.body.append(div)
div.innerText = 'Сентябрь'

let calendar = document.createElement("table");
document.body.append(calendar);
for (let j = 0; j < 7; j++) {
    let td = document.createElement("td");
    let days = [
    	'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']
    
    calendar.appendChild(td);
    td.innerText = days[j];
}
for (let i = 1; i <= 5; i++) {
  let tr = document.createElement("tr");

  calendar.appendChild(tr);
  tr.innerText = i;
}

