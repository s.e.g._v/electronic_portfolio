let board = [];
let rows = 12;
let columns = 12;

let minesCount = 25;
let minesLocation = [];

let blockClicked = 0;
let flagOn = false;

let gameOver = false;
let flag_index = 0;

window.onload = function() {
  startGame();
}


function setMines() {


  let minesLeft = minesCount;
  while (minesLeft > 0) {
    let r = Math.floor(Math.random() * rows);
    let c = Math.floor(Math.random() * columns);
    let id = r.toString() + '-' + c.toString();

    if (!minesLocation.includes(id)) {
      minesLocation.push(id);
      minesLeft -= 1;
    }
  }
}

setMines()


window.addEventListener('contextmenu', (ev) => { 
  ev.preventDefault(); 
  flag_index += 1;
  console.log(flag_index)
  if (flag_index % 2 == 0) {
    flagOn = true;
  }
  else if (flag_index % 2 != 0) {
    flagOn = false;
  }
  console.log(flagOn)
})



function startGame() {
  document.getElementById('mines_count').innerText = minesCount;

  for (let r = 0; r < rows; r++) {
    let row = [];
    for (let c = 0; c < columns; c++) {
      let block = document.createElement('div');
      block.id = r.toString() + '-' + c.toString();
      block.addEventListener('click', clickBlock);
      document.getElementById('game_board').append(block);
      row.push(block);
    }
    board.push(row);
  }
  console.log(board)
}


function clickBlock () {
  if (gameOver || this.style.backgroundColor == 'white') {
    return;
  }


  let block = this;
  if (flagOn) {
    if (block.innerText == '') {
      block.innerText = ' 🚩';
    }
    else if (block.innerText == ' 🚩') {
      block.innerText = '';
    }
    return;
  }
  if (minesLocation.includes(block.id)) {
    alert('YOU DIED');
    gameOver = true;
    minesShow();
    return;

  }


  let coords = block.id.split('-');
  let r = parseInt(coords[0])
  let c = parseInt(coords[1])
  checkMine(r, c)
}


function minesShow() {
  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < columns; c++) {
      let block = board[r][c];
      if (minesLocation.includes(block.id)) {
        block.innerText = '💣';
        block.style.backgroundColor = 'red';
      }
    }
  }
}


function checkMine(r, c) {
  if (r < 0 || r >= rows || c < 0 || c >= columns) {
    return;
  }
  if (board[r][c].style.backgroundColor == 'white') {
    return;
  }

  board[r][c].style.backgroundColor = 'white';


  blockClicked += 1;

  let minesFound = 0;

  minesFound += checkBlock(r - 1, c - 1);
  minesFound += checkBlock(r - 1, c);
  minesFound += checkBlock(r - 1, c + 1);

  minesFound += checkBlock(r, c - 1);
  minesFound += checkBlock(r, c + 1);


  minesFound += checkBlock(r + 1, c - 1);
  minesFound += checkBlock(r + 1, c);
  minesFound += checkBlock(r + 1, c + 1);

  if (minesFound > 0) {
    board[r][c].innerText = minesFound;
    board[r][c].style.color = 'blue'
  }
  else {
    checkMine(r-1, c-1)
    checkMine(r-1, c)
    checkMine(r-1, c+1)

    checkMine(r, c - 1);
    checkMine(r, c + 1);

    checkMine(r + 1, c - 1);
    checkMine(r + 1, c);
    checkMine(r + 1, c + 1);
  }


  if (blockClicked == rows * columns - minesCount) {
    document.getElementById('mines_count').innerText = 'нет';
    gameOver = true;
  }

}


function checkBlock(r, c) {
  if (r < 0 || r >= rows || c < 0 || c >= columns) {
    return 0;
  }
  if (minesLocation.includes(r.toString() + '-' + c.toString())) {
    return 1;
  }
  return 0;

}