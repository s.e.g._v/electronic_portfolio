import pygame
import math
from math import sqrt
from math import *
from pygame import mixer

pygame.init()

pygame.display.set_caption('Solar System')
WIDTH = 1000
HEIGHT = 1000
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
BACKGROUND = pygame.image.load('SolarContent/Universe.png')
mixer.music.load('SolarContent/Atlas.mp3')
mixer.music.play()


class Planet:
    def __init__(self, col, rad_planet, x_pos, y_pos, speed, orbit_width, name):
        self.name = name
        self.col = col
        self.rad = rad_planet
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.speed = speed
        self.a = radians(270)
        self.orbit_width = orbit_width
        self.rad_orbit = sqrt((self.x_pos - WIDTH / 2) ** 2 + (self.y_pos - HEIGHT / 2) ** 2)
        self.orbit = []

    def draw(self, WINDOW):
        if len(self.orbit) > 2:
            # for point in self.orbit:
            pygame.draw.lines(WINDOW, self.col, False, self.orbit, self.orbit_width)
        pygame.draw.circle(WINDOW, self.col, (self.x_pos, self.y_pos), self.rad)

    def update(self):
        self.x_pos = self.rad_orbit * math.cos(self.a) + WIDTH / 2
        self.y_pos = self.rad_orbit * math.sin(self.a) + HEIGHT / 2
        self.a += self.speed
        self.orbit.append((self.x_pos, self.y_pos))


def game():
    running = True
    fps = pygame.time.Clock()

    Sun = Planet((255, 255, 255), 30, WIDTH / 2, HEIGHT / 2, 20, 0, 'Sun')
    Mercury = Planet((192, 239, 74), 4.8, 420, 420, 0.1, 1, 'Mercury')
    Venus = Planet((255, 112, 67), 12.1, 400, 400, 0.08, 2, 'Venus')
    Earth = Planet((0, 188, 212), 12.7, 380, 380, 0.07, 2, 'Earth')
    Mars = Planet((204, 141, 26), 6.7, 360, 360, 0.05, 1, 'Mars')
    Jupiter = Planet((120, 144, 156), 30, 310, 310, 0.02, 3, 'Jupiter')
    Saturn = Planet((255, 245, 157), 25, 260, 260, 0.015, 3, 'Saturn')
    Uran = Planet((179, 229, 252), 16, 210, 210, 0.01, 3, 'Uran')
    Neptun = Planet((190, 212, 251), 15, 160, 160, 0.005, 3, 'Neptun')
    Pluton = Planet((225, 242, 249), 2, 110, 110, 0.003, 1, 'Pluton')

    planets = [Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uran, Neptun, Pluton]

    while running:
        fps.tick(40)
        WINDOW.fill((0, 0, 0))
        WINDOW.blit(BACKGROUND, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEMOTION:
                x, y = event.pos

                for i in planets:
                    if ((i.x_pos - 60) <= x <= (i.x_pos + 60)) and ((i.y_pos - 60) <= y <= (i.y_pos + 60)):
                        planet_name = pygame.font.Font(None, 30)
                        text_01 = planet_name.render(i.name, True, (255, 255, 255))
                        WINDOW.blit(text_01, (i.x_pos - 45, i.y_pos + 60))

                        grad = str(degrees(cos(sqrt((i.x_pos - WIDTH / 2) ** 2 + (i.y_pos - HEIGHT / 2) ** 2))))
                        planet_desc = pygame.font.Font(None, 25)
                        text_02 = planet_desc.render(grad, True, (255, 255, 255))
                        text_03 = planet_desc.render('Угол -', True, (255, 255, 255))
                        WINDOW.blit(text_02, (130, 950))
                        WINDOW.blit(text_03, (50, 950))

        Sun.draw(WINDOW)
        for planet in planets:
            planet.draw(WINDOW)
            planet.update()

        pygame.display.update()

    pygame.quit()


pygame.display.update()
game()
