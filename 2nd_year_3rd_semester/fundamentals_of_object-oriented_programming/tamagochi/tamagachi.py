from datetime import datetime as dt
import time
import pygame
from tkinter import *

pygame.init()

pygame.display.set_caption('Tamagachi')
WIDTH = 1000
HEIGHT = 1000
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)


class Animal:
    __states = {0: 'Мертвый', 1: 'Голодный', 2: 'Сытый', 3: 'Нажравшийся'}

    def __init__(self, name="Серега"):
        self.__play = 0
        self.__amount = 0
        self.__name = name
        self.__st = dt.now()
        self.__state = Animal.__states[3]
        self.__start_time = time.time()  # время, когда покушал
        print('Сброс голода', self.__start_time)

    def eat(self):
        self.__amount += 1
        self.__start_time += 5
        print("Дал еды", self.__amount)
        self.__play = 0

    def gamer(self):
        self.__play += 1
        print("Дал нежности", self.__play)
        self.__start_time += 2

    def killer(self):
        self.__start_time -= 20000
        print("Порезал на куски", self.__amount)

    def state(self):

        end_time = time.time()  # время без еды на каждый кадр

        if self.__amount <= 20:

            if self.__play <= 5:

                if end_time - self.__start_time < 10:
                    return Animal.__states[3]
                elif end_time - self.__start_time < 20:
                    return Animal.__states[2]

                elif end_time - self.__start_time < 30:
                    return Animal.__states[1]
                else:
                    return Animal.__states[0]
            else:
                self.__play = 5
                self.__start_time -= 2

        else:
            return Animal.__states[0] + ' (умер от переедания)'

    @property
    def amount(self):
        return self.__amount

    @property
    def play(self):
        return self.__play


def game():
    a = Animal()
    running = True
    fps = pygame.time.Clock()

    while running:
        WINDOW.fill(BLACK)
        fps.tick(40)

        # Состояние животного

        animal_state = pygame.font.Font(None, 40)
        text_animal_state = animal_state.render(a.state(), True, WHITE)
        WINDOW.blit(text_animal_state, (400, 100))

        # Инструкция к игре

        description = pygame.font.Font(None, 30)
        text_description = description.render('Чтобы что-то сделать со зверем - выберите опцию в виде значка', True,
                                              WHITE)
        WINDOW.blit(text_description, (160, 50))

        # Количество еды

        property_amount = pygame.font.Font(None, 30)
        text_property_amount = property_amount.render('Количество еды в миске: ' + str(a.amount), True,
                                                      WHITE)
        WINDOW.blit(text_property_amount, (380, 900))

        # Сколько раз поиграл с тварью

        property_play = pygame.font.Font(None, 30)
        text_property_play = property_play.render('Дал нежности: ' + str(a.play), True, WHITE)
        WINDOW.blit(text_property_play, (420, 800))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                print(event.pos)
                if (400 <= x <= 600) and (420 <= y <= 570):
                    a.eat()
                    print('Покормил!')
                if (156 <= x <= 248) and (456 <= y <= 570):
                    a.gamer()
                    print('Поиграл!')
                if (767 <= x <= 837) and (459 <= y <= 538):
                    a.killer()
                    print('Убил!')

        animal_live_surf = pygame.image.load("TamagachiContent/animal_live.png").convert_alpha()
        animal_live_rect = animal_live_surf.get_rect(center=(WIDTH / 2, HEIGHT / 2))

        animal_game_surf = pygame.image.load("TamagachiContent/animal_game.png").convert_alpha()
        animal_game_rect = animal_game_surf.get_rect(center=(200, HEIGHT / 2))

        animal_kill_surf = pygame.image.load("TamagachiContent/animal_kill.png").convert_alpha()
        animal_kill_rect = animal_kill_surf.get_rect(center=(800, HEIGHT / 2))

        WINDOW.blit(animal_live_surf, animal_live_rect)
        WINDOW.blit(animal_game_surf, animal_game_rect)
        WINDOW.blit(animal_kill_surf, animal_kill_rect)

        pygame.display.update()

    pygame.quit()


game()
