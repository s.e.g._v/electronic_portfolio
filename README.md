<h1 align="center">Electronic Portfolio</h1>

# Резюме (RU)

## Образование:

Студент ИГУ, Факультет бизнес-коммуникаций и информатики, направление - 
прикладная информатика в дизайне


## Профессиональный навыки:

- Конкурентный 
- Oриентированный на бизнес
- Быстрообучаемый
- Коммуникабельный
- Ответственный
- Дружелюбный
- Владение английским: B1 (пороговый)


## Знание программ:

1) 3Д Дизайн и Анимация:

    - 3DS Max
    - Blender
    - Keyshot
    - Fusion 360
    - ZBrush
    - Vray
    - Corona

2) Графический дизайн и медиа:

    - Photoshop
    - CorelDraw
    - Figma
    - Animate

3) Веб и разработка приложений

    - HTML
    - CSS 
    - Javascript 
    - SQL
    - QML
    - PHP

4) Прочее:

    - TouchDesigner


## Знание языков программирования:

1) Python - продвинутый

2) Kotlin - базовый



# Резюме (EN)

## Education:

ISU student, Faculty of Business Communications and Computer Science, direction - 
applied computer science in design


## Professional skills:

- Competitive
- Business-oriented
- Quick-learning
- Sociable
- Responsible
- Friendly
- English proficiency: B1 (Intermediate)


## Knowledge of programs:

1) 3D Design & Animation:

    - 3DS Max
    - Blender
    - Keyshot
    - Fusion 360
    - ZBrush
    - Vray
    - Corona

2) Graphic design & Media:

    - Photoshop
    - CorelDraw
    - Figma
    - Animate

3) Web & Application development:

    - HTML
    - CSS 
    - Javascript 
    - SQL
    - QML
    - PHP

4) Other:

    - TouchDesigner


## Knowledge of programming languages:

1) Python - advanced

2) Kotlin - basic

# Ссылки/Links

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/1st_year_1st_semester/introduction_to_programming?ref_type=heads">Introduction to programming</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/1st_year_1st_semester/markup%20languages?ref_type=heads">Markup 
Languages (first semester)</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/1st_year_2nd_semester/markup%20languages">Markup 
Languages (second semester)</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/1st_year_2nd_semester/programming?ref_type=heads">Programming</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/2nd_year_3rd_semester/fundamentals_of_object-oriented_programming?ref_type=heads">Fundamentals of object-oriented programming
</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/2nd_year_3rd_semester/client-side_programming">Client-side programming
</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/2nd_year_4th_semester/server-side%20programming?ref_type=heads">Server-side programming
</a></h1>

<h1 align="center"><a href="https://gitlab.com/s.e.g._v/electronic_portfolio/-/tree/main/3rd_year_5th_semester/graphical_interfaces_design?ref_type=heads">Graphical interfaces design
</a></h1>





