import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id: win
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Block {
        cubicColor: "red"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110
        anchors.bottomMargin: 110
    }

    Block {
        cubicColor: "red"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 2
        anchors.bottomMargin: 110
    }

    Block {
        cubicColor: "red"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 3
        anchors.bottomMargin: 110
    }

    Block {
        cubicColor: "red"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 4
        anchors.bottomMargin: 110
    }

    Block {
        cubicColor: "red"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 5
        anchors.bottomMargin: 110
    }

    Block {
        cubicColor: "yellow"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 + 55
        anchors.bottomMargin: 110 * 2
    }

    Block {
        cubicColor: "yellow"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 + 165
        anchors.bottomMargin: 110 * 2
    }

    Block {
        cubicColor: "yellow"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 + 275
        anchors.bottomMargin: 110 * 2
    }

    Block {
        cubicColor: "yellow"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 + 385
        anchors.bottomMargin: 110 * 2
    }

    Block {
        cubicColor: "yellow"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 + 385
        anchors.bottomMargin: 110 * 2
    }

    Block {
        cubicColor: "blue"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 2
        anchors.bottomMargin: 110 * 3
    }

    Block {
        cubicColor: "blue"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 3
        anchors.bottomMargin: 110 * 3
    }

    Block {
        cubicColor: "blue"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 4
        anchors.bottomMargin: 110 * 3
    }

    Block {
        cubicColor: "green"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 2 + 55
        anchors.bottomMargin: 110 * 4
    }

    Block {
        cubicColor: "green"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 110 * 2 + 165
        anchors.bottomMargin: 110 * 4
    }

}
