#include "qml_01_02.h"

QML_01_02::QML_01_02(QObject *parent)
    : QAbstractItemModel(parent)
{
}

QVariant QML_01_02::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}

QModelIndex QML_01_02::index(int row, int column, const QModelIndex &parent) const
{
    // FIXME: Implement me!
}

QModelIndex QML_01_02::parent(const QModelIndex &index) const
{
    // FIXME: Implement me!
}

int QML_01_02::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

int QML_01_02::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

QVariant QML_01_02::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}
