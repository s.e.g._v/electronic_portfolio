import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id: display
    visible: true
    width: 480
    height: 640
    title: qsTr("Task_for_Layout")

    Rectangle {
        id: header

        color: "grey"
        width: parent.width
        height: parent.height / 8

        anchors.top: parent.top

        Text {
            text: "Header"

            anchors.centerIn: header
        }

    }

    Rectangle {
        id: body

        color: "white"
        width: parent.width
        height: parent.height / 1.35

        anchors.top: header.bottom

    }

    Rectangle {

        id: body_content
        border.width: 2
        border.color: "gray"

        color: "white"

        width: body.width - 15
        height: body.height - 15

        anchors.centerIn: body

        Text {
            text: "Content"

            anchors.centerIn: body_content
        }

    }

    Rectangle {
        id: button1

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        Text {
            text: "1"

            anchors.centerIn: button1
        }

    }

    Rectangle {
        id: button2

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            text: "2"

            anchors.centerIn: button2
        }

    }

    Rectangle {
        id: button3

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        Text {
            text: "3"

            anchors.centerIn: button3
        }

    }
}
