import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15

ApplicationWindow {
    id: win

    width: 640
    height: 640
    visible: true
    title: qsTr("Task_for_Login_Page")

    ColumnLayout {

        id: display


        height: 100
        width: 300

        anchors.centerIn: parent


        TextField {

            id: username

            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            placeholderText: "Username"
            font.pixelSize: 16


        }
        TextField {

            id: passwordField

            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true

            placeholderText: "Password"
            font.pixelSize: 16
            echoMode: TextInput.Password
        }

        RowLayout {

            Layout.alignment: Qt.AlignHCenter

            Button {
                id: login
                Layout.fillWidth: true
                text: "Log in"
                font.pixelSize: 16
                height: 300
                width: 300
            }

            Button {

                id: clear

                Layout.fillWidth: true
                text: "Clear"
                font.pixelSize: 16
                height: 300
                width: 300

                MouseArea {
                    anchors.fill: clear

                    onClicked: {
                        passwordField.text = ""
                        username.text = ""
                    }
                }


            }
        }
    }
}
