import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 640
    visible: true
    title: qsTr("Hello World")

    Column {

        id: display

        height: 100
        width: 300

        anchors.centerIn: parent

        Text {
            id: passwordField
            text: passwordField.text
            color: "transparent"
        }


        Text {
            text: "Enter your password:"
            font.pixelSize: 16
            Layout.alignment: Qt.AlignCenter
        }


        Rectangle {

            id: passwordField1

            color: "white"
            width: parent.width
            height: 50
            Layout.alignment: Qt.AlignCenter
            Row {
                spacing: 6
                anchors.centerIn: parent
                // Добавляем 6 элементов Label для отображения введенных символов
                Repeater {
                    model:6
                    Label {
                        width: 20
                        height: 20
                        font.pixelSize: 36
                        text: "*"
                        Layout.alignment: Qt.AlignCenter
                        color:index <passwordField.text.length ? "black" : "light grey"
                    }
                }
            }
        }


        GridLayout {


            id: keypad
            rows: 4
            columns: 3
            width: parent.width

            columnSpacing : 20
            rowSpacing : 20




        Button {

            id: btn1
            text: "1"
            onClicked: passwordField.text+= "1"
            Layout.fillWidth: true

        }

        Button {

            id: btn2
            text: "2"
            onClicked: passwordField.text+= "2"
            Layout.fillWidth: true
        }

        Button {

            id: btn3
            text: "3"
            onClicked: passwordField.text+= "3"
            Layout.fillWidth: true
        }

        Button {

            id: btn4
            text: "4"
            onClicked: passwordField.text+= "4"
            Layout.fillWidth: true
        }
        Button {

            id: btn5
            text: "5"
            onClicked: passwordField.text+= "5"
            Layout.fillWidth: true
        }
        Button {

            id: btn6
            text: "6"
            onClicked: passwordField.text+= "6"
            Layout.fillWidth: true
        }
        Button {

            id: btn7
            text: "7"
            onClicked: passwordField.text+= "7"
            Layout.fillWidth: true
        }
        Button {

            id: btn8
            text: "8"
            onClicked: passwordField.text+= "8"
            Layout.fillWidth: true
        }
        Button {

            id: btn9
            text: "9"
            onClicked: passwordField.text+= "9"
            Layout.fillWidth: true
        }
        Button {

            id: btnnull
            text: ""
            onClicked: passwordField.text+= ""
            Layout.fillWidth: true
        }
        Button {

            id: btn0
            text: "0"
            onClicked: passwordField.text+= "0"
            Layout.fillWidth: true
        }
        Button {

            id: clear
            text: "Clear"
            onClicked: passwordField.text = ""
            Layout.fillWidth: true
        }
        }

        Button {

            id: login
            text: "Log in"
            width: 80
            height: 30
        }
    }
}
