import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id: display
    visible: true
    width: 480
    height: 640
    title: qsTr("Task_for_Signal")

    Rectangle {
        id: header

        color: "grey"
        width: parent.width
        height: parent.height / 8

        anchors.top: parent.top

        Text {
            id: header_text
            text: "Header"

            anchors.centerIn: header
        }

    }

    Rectangle {
        id: body

        color: "white"
        width: parent.width
        height: parent.height / 1.35

        anchors.top: header.bottom

    }

    Rectangle {

        id: body_content
        border.width: 2
        border.color: "gray"

        color: "white"

        width: body.width - 15
        height: body.height - 15

        anchors.centerIn: body

        Text {
            id: body_content_text
            text: "Some content"

            anchors.centerIn: body_content
        }

    }

    Rectangle {
        id: button1

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        Text {
            id: button1_text
            text: "1"

            anchors.centerIn: button1
        }

        MouseArea {
            id: mouse
            anchors.fill: button1

            onPressed: {
                body_content_text.text = "Item 1 content"
                header_text.text = "Header 1"

                button2_text.opacity = 0.4
                button3_text.opacity = 0.4
                button1_text.opacity = 1

                button2.opacity = 0.7
                button3.opacity = 0.7
                button1.opacity = 1

            }

        }

    }

    Rectangle {
        id: button2

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: button2_text
            text: "2"

            anchors.centerIn: button2

        }

        MouseArea {
            id: mouse_02
            anchors.fill: button2

            onPressed: {
                body_content_text.text = "Item 2 content"
                header_text.text = "Header 2"

                button1_text.opacity = 0.4
                button3_text.opacity = 0.4
                button2_text.opacity = 1

                button1.opacity = 0.7
                button3.opacity = 0.7
                button2.opacity = 1

            }

        }

    }

    Rectangle {
        id: button3

        color: "gray"
        width: parent.width/3 - 5

        anchors.top: body.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        Text {
            id: button3_text
            text: "3"

            anchors.centerIn: button3
        }

        MouseArea {
            id: mouse_03
            anchors.fill: button3

            onPressed: {
                body_content_text.text = "Item 3 content"
                header_text.text = "Header 3"

                button1_text.opacity = 0.4
                button3_text.opacity = 1
                button2_text.opacity = 0.4

                button1.opacity = 0.7
                button3.opacity = 1
                button2.opacity = 0.7

            }

        }

    }
}

