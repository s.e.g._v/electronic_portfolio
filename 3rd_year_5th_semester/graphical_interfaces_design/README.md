# О предмете (RU)

QML (Qt Meta Language или Qt Modeling Language) — декларативный язык программирования, в основании которого лежит среда JavaScript.

QML используется для разработки приложений, делающих основной упор на пользовательский интерфейс. Является частью Qt Quick, среды разработки пользовательского интерфейса

# About subject (ENG)

QML (Qt Meta Language or Qt Modeling Language) is a declarative programming language based on the JavaScript environment.

QML is used to develop applications that focus on the user interface. It is part of Qt Quick, a user interface development environment


## Демонстрация работы по заданиям

    1) QML_01_01
![1](gifs/QML_01_01.gif "QML_01_01")

    2) QML_01_02
![2](gifs/QML_01_02.png "QML_01_02")

    3) QML_04
![3](gifs/QML_04.gif "QML_04")

    4) QML_05_01
![4](gifs/QML_05_01.gif "QML_05_01")

    5) QML_05_02
![5](gifs/QML_05_02.gif "QML_05_02")

    6) QML_07
![6](gifs/QML_07.gif "QML_07")

    7) QML_07_02
![7](gifs/QML_07_02.gif "QML_07_02")


