import QtQuick 2.15
import QtQuick.Window 2.15

Window {

 width: 480
 height: 640
 visible: true
 title: qsTr("Hello World")

 minimumWidth: 360
 minimumHeight: 480

 Rectangle {

     id: rect

     width: 250
     height: 450
     color:"lightgray"
     anchors.centerIn: parent

     states:[
         State {
             name: "stop"
             PropertyChanges {
                 target: rect_red
                 color: "red"

             }
             PropertyChanges {
                 target:rect_yellow
                 color: "black"
             }
             PropertyChanges {
                 target:rect_green
                 color: "black"
             }
         },
         State {
             name: "caution"
             PropertyChanges {
                 target: rect_red
                 color: "black"
             }
             PropertyChanges {
                 target:rect_yellow
                 color: "yellow"
             }
             PropertyChanges {
                 target:rect_green
                 color: "black"
             }
         },
         State {
             name: "go"
             PropertyChanges {
                 target: rect_red
                 color: "black"
             }
             PropertyChanges {
                 target:rect_yellow
                 color: "black"
             }
             PropertyChanges {
                 target:rect_green
                 color: "green"
             }
         } ]

     state:"stop"



     transitions:[
         Transition {
             from: "stop"
             to: "caution"


             ColorAnimation {
                 target: rect_red
                 from: "red"
                 to: "black"
                 duration: 1000
             }

             ColorAnimation {
                 target: rect_yellow
                 from: "black"
                 to: "yellow"
                 duration: 1000
             }

         },
         Transition {
             from: "caution"
             to: "go"

             ColorAnimation {
                 target: rect_yellow
                 from: "yellow"
                 to: "black"
                 duration: 1000
             }

             ColorAnimation {
                 target: rect_green
                 from: "black"
                 to: "green"
                 duration: 1000
             }
         },
         Transition {
             from: "go"
             to: "stop"

             ColorAnimation {
                 target: rect_green
                 from: "green"
                 to: "black"
                 duration: 1000
             }

             ColorAnimation {
                 target: rect_red
                 from: "black"
                 to: "red"
                 duration: 1000
             }
         }
     ]

     // Обработчик смены цвета при нажатии на светофор

     MouseArea {
         anchors.fill: parent

         onClicked: {
             if (parent.state === "stop") {
                 parent.state = "caution"
             } else if (parent.state === "caution") {
                 parent.state = "go"
             } else {
             parent.state = "stop"}
         }
     }

     // Светофор

     Rectangle {

         id:rect_red

         width: 100
         height: 100
         color:"red"
         anchors.top: rect.top
         anchors.horizontalCenter: rect.horizontalCenter

         anchors.topMargin: 40
     }


     Rectangle {

         id:rect_yellow

         width: 100
         height: 100
         color:"yellow"
         anchors.centerIn: parent
     }

     Rectangle {

         id:rect_green

         width: 100
         height: 100
         color:"green"
         anchors.bottom: rect.bottom
         anchors.horizontalCenter: rect.horizontalCenter

         anchors.bottomMargin: 40
     }
 }
}
