import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id: display
    visible: true
    width: 480
    height: 640
    title: qsTr("Task_for_Signal")

    Item {

        id: wrapper

        anchors.fill: parent

        states:[
            State {
                name: "default"
                PropertyChanges {
                    target: item1
                }
                PropertyChanges {
                    target: item2
                }
                PropertyChanges {
                    target: item3
                }
                PropertyChanges {
                    target: body_content_text
                    text: "Some content"
                }
                PropertyChanges {
                    target: header_text
                    text: "Header"
                }
                PropertyChanges {
                    target: go_back
                    opacity: 0
                }
            },
            State {
                name: "item1"
                PropertyChanges {
                    target: item1
                }
                PropertyChanges {
                    target: item2
                    opacity: 0.7
                }
                PropertyChanges {
                    target: item3
                    opacity: 0.7
                }
                PropertyChanges {
                    target: body_content_text
                    text: "Item 1 content"
                }
                PropertyChanges {
                    target: header_text
                    text: "Header 1"
                }
                PropertyChanges {
                    target: go_back
                    opacity: 0
                }
            },
            State {
                name: "item2"
                PropertyChanges {
                    target: item1
                    opacity: 0.7
                }
                PropertyChanges {
                    target: item2
                    opacity: 1
                }
                PropertyChanges {
                    target: item3
                    opacity: 0.7
                }
                PropertyChanges {
                    target: body_content_text
                    text: "Item 2 content"
                }
                PropertyChanges {
                    target: header_text
                    text: "Header 2"
                }
            },
            State {
                name: "item3"
                PropertyChanges {
                    target: item1
                    opacity: 0.7
                }
                PropertyChanges {
                    target: item2
                    opacity: 0.7
                }
                PropertyChanges {
                    target: item3
                    opacity: 1
                }
                PropertyChanges {
                    target: body_content_text
                    text: "Item 3 content"
                }
                PropertyChanges {
                    target: header_text
                    text: "Header 3"
                }
            }
        ]

        state:"default"

        transitions: [
            Transition {
                PropertyAnimation { properties: "opacity"; duration: 500; }

            }
        ]





        // Разметка документа

        Rectangle {
            id: header

            color: "grey"
            width: wrapper.width
            height: wrapper.height / 8

            anchors.top: wrapper.top

            Text {
                id: header_text
                text: "Header"

                anchors.centerIn: header
            }

            Rectangle {
                id: go_back

                width: header.width / 8
                height: header.height / 2

                color: "lightgray"

                anchors.left: header.left

                anchors.margins: 10

                Text {

                    id: go_back_text
                    text: "Back"

                    anchors.centerIn: go_back

                }

                MouseArea {
                    anchors.fill: go_back

                    onClicked: {
                        if (wrapper.state === "item2") {
                            wrapper.state = "item1"
                        } else if (wrapper.state === "item3") {
                            wrapper.state = "item2"
                        }
                    }
                }

            }

        }

        Rectangle {
            id: body

            color: "white"
            width: wrapper.width
            height: wrapper.height / 1.35

            anchors.top: header.bottom

        }

        Rectangle {

            id: body_content
            border.width: 2
            border.color: "gray"

            color: "white"

            width: body.width - 15
            height: body.height - 15

            anchors.centerIn: body

            Text {
                id: body_content_text
                text: "Some content"

                anchors.centerIn: body_content
            }



        }

        Rectangle {
            id: item1

            color: "gray"
            opacity: 1
            width: wrapper.width/3 - 5

            anchors.top: body.bottom
            anchors.bottom: wrapper.bottom
            anchors.left: wrapper.left

            Text {
                id: item1_text
                text: "Item 1"

                anchors.centerIn: item1
            }

            MouseArea {
                anchors.fill: item1

                onClicked: {
                    wrapper.state = "item1"
                }
            }

        }

        Rectangle {
            id: item2

            color: "gray"
            opacity: 1
            width: wrapper.width/3 - 5

            anchors.top: body.bottom
            anchors.bottom: wrapper.bottom
            anchors.horizontalCenter: wrapper.horizontalCenter

            Text {
                id: item2_text
                text: "Item 2"

                anchors.centerIn: item2

            }

            MouseArea {
                anchors.fill: item2

                onClicked: {
                    wrapper.state = "item2"
                }
            }

        }

        Rectangle {
            id: item3

            color: "gray"
            opacity: 1
            width: wrapper.width/3 - 5

            anchors.top: body.bottom
            anchors.bottom: wrapper.bottom
            anchors.right: wrapper.right

            Text {
                id: item3_text
                text: "Item 3"

                anchors.centerIn: item3
            }

            MouseArea {
                anchors.fill: item3

                onClicked: {
                    wrapper.state = "item3"
                }
            }

        }
    }

}


