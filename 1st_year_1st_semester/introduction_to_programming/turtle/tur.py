import turtle as t

turtle = t.Turtle()
turtle.shape(name="square")
turtle.color("black")
turtle.pencolor("brown")
turtle.speed(0)
turtle.shapesize(0.2)
e = 0
u = 0

for i in range(170):
    for r in range(5):
        turtle.forward(e)
        turtle.left(320)
        turtle.forward(e)
        turtle.width(u)
    u += 0.02
    e += 0.2
    turtle.width(u)
    turtle.forward(80)
    turtle.right(130)
turtle.done()
