import turtle as t
beck = t.Screen()
beck.bgcolor("black")
beck.tracer(1)
ball = t.Turtle()
ball.shape("circle")
ball.color("red")
ball.penup()
ball.speed(0)
ball.goto(0, 200)
ball.dy = 0
ball.dx = 8
gravity = 0.1

while True:
    beck.update()
    ball.dy -= gravity
    ball.sety(ball.ycor() + ball.dy)
    ball.setx(ball.xcor() + ball.dx)
    if ball.xcor() > 380:
        ball.dx *= -1
    if ball.xcor() < -380:
        ball.dx *= -1
    if ball.ycor() < -380:
        ball.dy *= -1
beck.mainloop()