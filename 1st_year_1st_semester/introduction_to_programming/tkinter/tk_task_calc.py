from tkinter import *
def summa():
    a = float(EntryA.get())
    b = float(EntryB.get())

    result = str(a + b)
    EntryC.delete(0, END)
    EntryC.insert(0, result)

def cheslb():
        try:
            int(EntryB.get())
            ErrorB["text"] = 'Отлично, дальше'
        except ValueError:
            ErrorB["text"] = 'Вы ввели не число!'
def chesla():
    try:
        int(EntryA.get())
        ErrorA["text"] = 'Отлично, дальше'
    except ValueError:
        ErrorA["text"] = 'Вы ввели не число!'


root = Tk()
root.title('Произведение двух чисел')
root.geometry('800x600')

Label(root, text='Первое число').pack()
EntryA = Entry(root, width=10, font='Arial 16')
ErrorA = Label(root, text="Здесь будут фиксироваться ваши ошибки")
ErrorA.place(y=20, x=50)
EntryA.pack()
print(EntryA.get())
checks = Button(root, text="Enter", command=chesla)
checks.place(y=20, x=460)

Label(root, text='Второе число').pack()
EntryB = Entry(root, width=10, font='Arial 16')
ErrorB = Label(root, text="Здесь будут фиксироваться ваши ошибки")
ErrorB.place(y=75, x=50)
EntryB.pack()
print(EntryB.get())
checks = Button(root, text="Enter", command=cheslb)
checks.place(y=70, x=460)

EntryC = Entry(root, width=20, font='Arial 16')
EntryC.pack()

but = Button(root, text='Сложение', command=summa)
but.pack()

root.mainloop()
