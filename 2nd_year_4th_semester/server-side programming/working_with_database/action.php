<?php
$db = new PDO("mysql:dbname=homework;host=localhost", "sblinnik", "1234");

$registrations = [];

// Считывание строк из таблицы MySQL

$sql = "SELECT * FROM participants";
$stmt = $db->prepare($sql);
$stmt->execute();
$registrations = $stmt->fetchAll(PDO::FETCH_ASSOC);


// Удаление выбранных заявок из таблицы MySQL

if (isset($_POST['done'])) {
    if (isset($_POST['delete'])) {
        $deleteIds = $_POST['delete'];

        if (empty($deleteIds)) {
            $errorMessage = 'Выберите хотя бы одну заявку для удаления.';
        } else {
            foreach ($deleteIds as $id) {
                $sql = "DELETE FROM participants WHERE id = :id";
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':id', $id, PDO::PARAM_INT);
                $stmt->execute();
            }
        }
    } else {
        $errorMessage = 'Выберите хотя бы одну заявку для удаления.';
    }
}

// Аутентификация

session_start();

$adminLogin = 'sergey';
$adminPasswordHash = '81dc9bdb52d04dc20036dbd8313ed055'; // 1234

if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] !== true) {
    if (isset($_POST['login']) && isset($_POST['password'])) {
        $login = $_POST['login'];
        $password = $_POST['password'];

        if ($login === $adminLogin && md5($password) === $adminPasswordHash) {
            $_SESSION['authenticated'] = true;
        } else {
            $errorMessage = 'Неверный логин или пароль.';
        }
    }
}

if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    $_SESSION['authenticated'] = false;
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Заявки на конференцию</title>
</head>

<body>
    <?php if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] !== true) { ?>
        <form method="post">
            <label for="login">Логин:</label>
            <input type="text" id="login" name="login" required><br>

            <label for="password">Пароль:</label>
            <input type="password" id="password" name="password" required><br>

            <input type="submit" value="Войти">
        </form>
        <?php if (isset($errorMessage)) { ?>
            <p><?= $errorMessage ?></p>
        <?php } ?>
    <?php } else { ?>
        <form method="post">
            <table>
                <tr>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Конференция</th>
                    <th>Метод оплаты</th>
                    <th>Получать рассылку</th>
                    <th>Удалить</th>
                </tr>
                <?php foreach ($registrations as $registration) { ?>
                    <tr>
                        <td><?= $registration['name'] ?></td>
                        <td><?= $registration['lastname'] ?></td>
                        <td><?= $registration['email'] ?></td>
                        <td><?= $registration['tel'] ?></td>
                        <td><?= $registration['subject'] ?></td>
                        <td><?= $registration['payment'] ?></td>
                        <td><?= $registration['mailing'] ?></td>
                        <td>
                            <input type="checkbox" name="delete[]" value="<?= $registration['id'] ?>">
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php if (isset($errorMessage)) { ?>
                <p><?= $errorMessage ?></p>
            <?php } ?>
            <p><input type="submit" value="Удалить заявки" name="done"></p>
        </form>
        <form method="post">
            <input type="submit" value="Выход" name="logout">
        </form>
    <?php } ?>
</body>

</html>
