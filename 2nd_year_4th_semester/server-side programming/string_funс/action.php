<?php

$filename = './files/conference_registrations.txt';

$registrations = [];

$deleteArray = [];

// Считываем все заявки из файла

if (file_exists($filename)) {
	$lines = file($filename, FILE_IGNORE_NEW_LINES);
	foreach ($lines as $line) {
		$data = explode('|', $line);
		if (count($data) == 10) {
			echo '';
		} else {
			$registrations[] = [
				'name' => trim($data[0]),
				'surname' => trim($data[1]),
				'email' => trim($data[2]),
				'phone' => trim($data[3]),
				'conference' => trim($data[4]),
				'payment_method' => trim($data[5]),
				'newsletter' => trim($data[6]),
				'submission_date' => trim($data[7]),
				'ip_address' => trim($data[8]),
			];
		}
	}
}

// Считывание файла, перезапись массива, запись измененного массива в новый файл

if (isset($_POST['done'])) {
	//echo 'Кнопка была нажата <br>';

	for ($i = 0; $i < count($lines); $i++) {

		foreach ($_POST['delete'] as $key => $value) {
			if (trim($lines[$i]) == $value) {
				$lines[$i] = $value . '|1';
			}
		}

	}

	//echo implode('<br>', $lines);
	file_put_contents($filename,[implode(PHP_EOL, array_filter($lines)), "\n"]);

}


    //foreach ($_POST['delete'] as $key => $value) {

        //echo implode('', $registrations[$key]);
        //echo '<br>';
        //echo $value . '|1';
        //echo '<br>';


        //file_put_contents($filename, $value . '|1');
    //}

?>

<!DOCTYPE html>
<html>

<head>
	<title>Заявки на конференцию</title>
</head>

<body>
	<form method="post">
		<table>
			<tr>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Email</th>
				<th>Телефон</th>
				<th>Конференция</th>
				<th>Метод оплаты</th>
				<th>Получать рассылку</th>
				<th>Дата подачи заявки</th>
				<th>IP-адрес</th>
				<th>Удалить</th>
			</tr>
			<?php foreach ($registrations as $registration) { ?>
				<tr>
					<td><?= $registration['name'] ?></td>
					<td><?= $registration['surname'] ?></td>
					<td><?= $registration['email'] ?></td>
					<td><?= $registration['phone'] ?></td>
					<td><?= $registration['conference'] ?></td>
					<td><?= $registration['payment_method'] ?></td>
					<td><?= $registration['newsletter'] ?></td>
					<td><?= $registration['submission_date'] ?></td>
					<td><?= $registration['ip_address'] ?></td>
					<td>
						<input type="checkbox" name="delete[]" value="<?= implode("|",$registration) ?>">
					</td>
				</tr>
			<?php } ?>
		</table>
		<p><input type="submit" value="Удалить заявки" name="done"></p>
	</form>
</body>

</html>