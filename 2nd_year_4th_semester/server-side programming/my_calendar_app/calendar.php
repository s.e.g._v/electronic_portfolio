<?php

$error = '';

if (isset($_POST["send"])) {

  if (!empty($_POST["topic"]) &&
    !empty($_POST["type"]) &&
    !empty($_POST["location"]) &&
    !empty($_POST["datetime"]) &&
    !empty($_POST["duration"]) &&
    !empty($_POST["comment"])) {

    $error .= 'Задача успешно записана!';

    // Запись данных в БД

    $db = new PDO("mysql:dbname=homework;host=localhost", "sblinnik", "1234");
    $sql = "INSERT INTO calendar_tasks (task_theme, task_type, task_place, task_date, task_duration, task_comment) VALUES ('".$_POST['topic']."', '".$_POST['type']."', '".$_POST['location']."', '".$_POST['datetime']."', '".$_POST['duration']."', '".$_POST['comment']."')";
    $db->prepare($sql)->execute();
    
    //
}

else {
  $error .= 'Не все поля заполнены!';
}

}
?>




<!DOCTYPE html>
<html>
<head>
  <title>Мой календарь</title>
  <style>
    label {
      display: block;
      margin-top: 10px;
    }
  </style>
</head>
<body>
  <h1>Мой календарь</h1>
  
  <form method="post">
    <label for="topic">Тема:</label>
    <input type="text" id="topic" name="topic">
    
    <label for="type">Тип:</label>
    <select id="type" name="type">
      <option value="встреча">Встреча</option>
      <option value="звонок">Звонок</option>
      <option value="совещание">Совещание</option>
      <option value="дело">Дело</option>
    </select>
    
    <label for="location">Место:</label>
    <input type="text" id="location" name="location">
    
    <label for="datetime">Дедлайн задачи:</label>
    <input type="datetime-local" id="datetime" name="datetime">
    
    <label for="duration">Приоритет:</label>
    <input type="number" id="duration" name="duration">
    
    <label for="comment">Комментарий:</label>
    <textarea id="comment" name="comment"></textarea>

    <br>
    <input type="submit" value="Добавить" name="send">
    <br>
    <a href="tasks.php">Перейти к задачам</a>
    <br>
    <?=$error?>
  </form>
</body>
</html>
