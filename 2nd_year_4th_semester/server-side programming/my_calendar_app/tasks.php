<?php

$db = new PDO("mysql:dbname=homework;host=localhost", "sblinnik", "1234");

// Считывание строк из таблицы MySQL

$registrations = [];

$sql = "SELECT * FROM calendar_tasks";
$stmt = $db->prepare($sql);
$stmt->execute();
$registrations = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (isset($_POST["filter"])) {
  $filter = $_POST["filter"];
  $currentDateTime = new DateTime();

  // Просроченные задачи
  
  if ($filter === "overdue") {
    $overdueTasks = [];
    
    foreach ($registrations as $registration) {
      $taskDateTime = new DateTime($registration['task_date']);
      if ($currentDateTime > $taskDateTime) {
        $registration['task_status'] = "Просрочено";
        $overdueTasks[] = $registration;
      }
    }
    
    $registrations = $overdueTasks;
  }

  // Текущие задачи

  if ($filter === "current") {
    $currentTasks = [];
    
    foreach ($registrations as $registration) {
      $taskDateTime = new DateTime($registration['task_date']);
      if ($currentDateTime < $taskDateTime && $registration['task_status'] != 'Выполнено') {
        $currentTasks[] = $registration;
      }
    }
    
    $registrations = $currentTasks;
  }

  // Выполненные задачи

  if ($filter === "completed") {
    $completedTasks = [];

    foreach ($registrations as $registration) {
      if ($registration['task_status'] == 'Выполнено') {
        $completedTasks[] = $registration;
      }
    }

    $registrations = $completedTasks;
  }

  // Фильтр по дате

  if ($filter === "datetime") {
    $selectedDate = isset($_POST['dates']) ? $_POST['dates'] : '';
    $selectedDateTime = DateTime::createFromFormat('Y-m-d\TH:i', $selectedDate);
    $selectedDateTimeString = $selectedDateTime ? $selectedDateTime->format('Y-m-d H:i:s') : '';
    $dataTasks = [];

    foreach ($registrations as $registration) {
      $taskDateTime = new DateTime($registration['task_date']);
      $taskDateTimeString = $taskDateTime->format('Y-m-d H:i:s');
      if ($taskDateTimeString == $selectedDateTimeString) {
        $dataTasks[] = $registration;
      }
    }

    $registrations = $dataTasks;
  }
}

?>


<!DOCTYPE html>
<html>
<head>
  <title>Примеры задач</title>
  <style>
    table {
      width: 100%;
      border-collapse: collapse;
    }

    th, td {
      padding: 8px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }

    th {
      background-color: #f2f2f2;
    }

    button {
      margin-top: 10px;
    }
  </style>
</head>
<body>
  <h1>Лист задач</h1>

  <form method="post">
    <label for="filter">Фильтр:</label>
    <button type="submit" name="filter" value="all">Все задачи</button>
    <button type="submit" name="filter" value="current">Текущие задачи</button>
    <button type="submit" name="filter" value="completed">Выполненные задачи</button>
    <button type="submit" name="filter" value="overdue">Просроченные задачи</button>
    <input type="datetime-local" id="dates" name="dates">
    <button type="submit" name="filter" value="datetime">Фильтр по дате</button>
  </form>

  <table id="taskTable">
    <tr>
      <th>Тема</th>
      <th>Тип</th>
      <th>Место</th>
      <th>Дедлайн задачи</th>
      <th>Приоритет</th>
      <th>Комментарий</th>
    </tr>
    <?php foreach ($registrations as $registration) { ?>
      <tr>
        <td><a href="edit_form.php?id=<?= $registration['id'] ?>"><?= $registration['task_theme'] ?></a></td>
        <td><?= $registration['task_type'] ?></td>
        <td><?= $registration['task_place'] ?></td>
        <td><?= $registration['task_date'] ?></td>
        <td><?= $registration['task_duration'] ?></td>
        <td><?= $registration['task_comment'] ?></td>
      </tr>
    <?php } ?>
  </table>
  <a href="calendar.php">Перейти на главную</a>
</body>
</html>
