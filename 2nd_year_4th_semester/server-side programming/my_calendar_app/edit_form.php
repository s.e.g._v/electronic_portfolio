<?php

$db = new PDO("mysql:dbname=homework;host=localhost", "sblinnik", "1234");

if(isset($_GET['id'])) {
  $id = $_GET['id'];

  $sql = "SELECT * FROM calendar_tasks WHERE id = :id";
  $stmt = $db->prepare($sql);
  $stmt->bindParam(":id", $id);
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

} else {
  echo "ID не указан";
}

if (isset($_POST["updater"])) {
  echo 'Задача была успешно обновлена!';

  // Обновление данных в БД

  $sql = "UPDATE calendar_tasks SET task_theme = :theme, task_type = :type, task_place = :place, task_status = :status WHERE id = :id";
  $stmt = $db->prepare($sql);
  $stmt->bindParam(":theme", $_POST["topic"]);
  $stmt->bindParam(":type", $_POST["type"]);
  $stmt->bindParam(":place", $_POST["location"]);
  $stmt->bindParam(":status", $_POST["choice"]);
  $stmt->bindParam(":id", $id);
  $stmt->execute();
}

?>

<!DOCTYPE html>
<html>
<head>
  <title>Картока задачи</title>
</head>
<body>
  <h2>Редактирование задачи</h2>
  <form method="post">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <li>
      <label for="task_theme">Тема:</label>
      <input type="text" id="topic" name="topic" value="<?php echo $row['task_theme']; ?>"><br>
    </li>
    <li>
      <label for="task_type">Тип:</label>
      <input type="text" id="type" name="type" value="<?php echo $row['task_type']; ?>"><br>
    </li>
    <li>
      <label for="task_place">Место:</label>
      <input type="text" id="location" name="location" value="<?php echo $row['task_place']; ?>"><br>
    </li>
    <li>
      <label>Дата дедлайна:</label>
      <label><?= $row['task_date'] ?></label><br>
    </li>
    <li>
      <label>Приоритет:</label>
      <label><?= $row['task_duration'] ?></label><br>
    </li>
    <li>
      <label>Комментарий:</label>
      <label><?= $row['task_comment'] ?></label><br>
    </li>
    <li>
      <label for="task_status">Статус:</label>
      <select id="choice" name="choice">
        <option value="В процессе" <?php if ($row['task_status'] == 'В процессе') echo 'selected'; ?>>В процессе</option>
        <option value="Выполнено" <?php if ($row['task_status'] == 'Выполнено') echo 'selected'; ?>>Выполнено</option>
      </select><br>
    </li>
    <input type="submit" value="Обновить" name="updater">
    <a href="tasks.php">Перейти к задачам</a>
  </form>
</body>
</html>
