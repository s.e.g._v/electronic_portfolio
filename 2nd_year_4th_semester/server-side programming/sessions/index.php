<?php

$error = '';

$dname = '';
$dsurname = '';
$demail = '';
$dphone = '';

$conference = '';


if (isset($_POST["done"])) {

    $dname .= $_POST['user_name'];
    $dsurname .= $_POST['user_surname'];
    $demail .= $_POST['user_email'];
    $dphone .= $_POST['user_phone'];

    if (!empty($_POST["user_name"]) &&
        !empty($_POST["user_surname"]) &&
        !empty($_POST["user_email"]) &&
        !empty($_POST["user_phone"])
    ) {

        // ---- Счетчик заявок

    $file_name = 'counter.txt';
    file_exists($file_name) || file_put_contents($file_name, 0);
    $count = (int)file_get_contents($file_name);
    file_put_contents($file_name, ++$count);




    $directoryPath = './files';

    if (!file_exists($directoryPath)) {
        mkdir($directoryPath, 0777, true);
        echo '';
            //echo 'Директория создана успешно.';
    } else {
            //echo 'Директория уже существует.';
        echo '';
    }

    $filename = './files/conference_registrations.txt';
    $user_data = [
        $_POST['user_name'],
        $_POST['user_surname'],
        $_POST['user_email'],
        $_POST['user_phone'],
        $_POST['user_conferences'],
        $_POST['user_payment_method'],
        $_POST['user_yes'],
        date('Y-m-d H:i:s'),
        getenv('REMOTE_ADDR')
    ];
    $line = implode('|', $user_data) . PHP_EOL;
    file_put_contents($filename, $line, FILE_APPEND);

    $dname = '';
    $dsurname = '';
    $demail = '';
    $dphone = '';
}

if ($_POST['user_name'] == '' or $_POST['user_surname'] == '' or $_POST['user_email'] == '' or $_POST['user_phone'] == '') {
    $error .= 'Не все поля заполнены!';
} else {
    $error .= 'Ваша заявка отправлена!';
}
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>Моя конференция</title>
</head>
<body>
  <form action="" method="post">

     <p><input type="text" id="name" name="user_name" placeholder="Введите имя" value="<?=$dname?>"></p>

     <p><input type="text" id="surname" name="user_surname" placeholder="Введите фамилию" value="<?=$dsurname?>"></p>

     <p><input type="text" id="email" name="user_email" placeholder="Введите адрес электронной почты" value="<?=$demail?>"></p>

     <p><input type="text" id="phone" name="user_phone" placeholder="Введите свой номер телефона" value="<?=$dphone?>"></p>

     <div>
        <label for="conference">Выберите тематику конференции</label>
        <select id="conference" name="user_conferences">
           <option value="Бизнес" <?php if (isset($_POST["user_conferences"]) && $_POST['user_conferences'] == 'Бизнес') {echo "selected";} ?>> Бизнес </option>
           <option value="Технологии" <?php if(isset($_POST["user_conferences"]) && $_POST['user_conferences'] == 'Технологии') {echo "selected";}?>>Технологии</option>
           <option value="Реклама и маркетинг" <?php if(isset($_POST["user_conferences"]) && $_POST['user_conferences'] == 'Реклама и маркетинг') {echo "selected";}?>>Реклама и маркетинг</option>
       </select>
   </div>

   <div>
       <label for="payment_method">Предпочитаемый метод оплаты</label>
       <select id="payment_method" name="user_payment_method">
          <option value="WebMoney" <?php if (isset($_POST["user_payment_method"]) && $_POST['user_payment_method'] == 'WebMoney') {echo "selected";}?>>WebMoney</option>
          <option value="Яндекс.Деньги" <?php if (isset($_POST["user_payment_method"]) && $_POST['user_payment_method'] == 'Яндекс.Деньги') {echo "selected";}?>>Яндекс.Деньги</option>
          <option value="PayPal" <?php if (isset($_POST["user_payment_method"]) && $_POST['user_payment_method'] == 'PayPal') {echo "selected";}?>>PayPal</option>
          <option value="Кредитная карта" <?php if (isset($_POST["user_payment_method"]) && $_POST['user_payment_method'] == 'Кредитная карта') {echo "selected";}?>>Кредитная карта</option>
      </select>
  </div>

  <div>
   <label>Получать рассылку о конференции?</label><br>
   <input type="hidden" name="user_yes" value="">
   <input type="radio" id="Получить рассылку" value="Получить рассылку" name="user_yes"><label for="Получить рассылку" class="light">Да</label><br>
</div>

<p><input type="submit" value="Отправить заявку" name="done"></p>
<?=$error?>

</form>
</body>
</html>
