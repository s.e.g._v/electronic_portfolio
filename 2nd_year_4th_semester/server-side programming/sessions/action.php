<?php

$filename = './files/conference_registrations.txt';

$registrations = [];

// Считываем все заявки из файла

if (file_exists($filename)) {
    $lines = file($filename, FILE_IGNORE_NEW_LINES);
    foreach ($lines as $line) {
        $data = explode('|', $line);
        if (count($data) == 10) {
            echo '';
        } else {
            $registrations[] = [
                'name' => trim($data[0]),
                'surname' => trim($data[1]),
                'email' => trim($data[2]),
                'phone' => trim($data[3]),
                'conference' => trim($data[4]),
                'payment_method' => trim($data[5]),
                'newsletter' => trim($data[6]),
                'submission_date' => trim($data[7]),
                'ip_address' => trim($data[8]),
            ];
        }
    }
}

// Считывание файла, перезапись массива, запись измененного массива в файл

if (isset($_POST['done'])) {
    if (isset($_POST['delete'])) {
        foreach ($_POST['delete'] as $key => $value) {
            for ($i = 0; $i < count($lines); $i++) {
                if (trim($lines[$i]) == $value) {
                    $lines[$i] = $value . '|1';
                }
            }
        }
    } else {
        $noSelectionError = 'Выберите хотя бы один пункт для удаления.';
    }

    file_put_contents($filename, [implode(PHP_EOL, array_filter($lines)), "\n"]);
}


// Подсчет посетителей уникальных по IP

$uniqueIPs = [];

foreach ($registrations as $registration) {
    $ip = $registration['ip_address'];
    if (!in_array($ip, $uniqueIPs)) {
        $uniqueIPs[] = $ip;
    }
}

$totalUniqueVisitors = count($uniqueIPs);

// Подсчет пользователей по сессиям

$totalUsers = count($registrations);

// Подсчет загрузок страницы (хитов)

$totalPageHits = count($lines);

// Аутентификация

session_start();

$adminLogin = 'sergey';
$adminPasswordHash = '81dc9bdb52d04dc20036dbd8313ed055'; // 1234

if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] !== true) {
    if (isset($_POST['login']) && isset($_POST['password'])) {
        $login = $_POST['login'];
        $password = $_POST['password'];

        if ($login === $adminLogin && md5($password) === $adminPasswordHash) {
            $_SESSION['authenticated'] = true;
        } else {
            $errorMessage = 'Неверный логин или пароль.';
        }
    }
}

if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    $_SESSION['authenticated'] = false;
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Заявки на конференцию</title>
</head>

<body>
    <?php if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] !== true) { ?>
        <form method="post">
            <label for="login">Логин:</label>
            <input type="text" id="login" name="login" required><br>

            <label for="password">Пароль:</label>
            <input type="password" id="password" name="password" required><br>

            <input type="submit" value="Войти">
        </form>
        <?php if (isset($errorMessage)) { ?>
            <p><?= $errorMessage ?></p>
        <?php } ?>
    <?php } else { ?>
        <p>Статистика:</p>
        <p>Посетители уникальные по IP: <?= $totalUniqueVisitors ?></p>
        <p>Пользователи по сессиям: <?= $totalUsers ?></p>
        <p>Загрузки страницы (хиты): <?= $totalPageHits ?></p>
        <br>
        <form method="post">
            <?php if (!empty($noSelectionError)) { ?>
                <p><?= $noSelectionError ?></p>
            <?php } ?>
            <table>
                <tr>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Конференция</th>
                    <th>Метод оплаты</th>
                    <th>Получать рассылку</th>
                    <th>Дата подачи заявки</th>
                    <th>IP-адрес</th>
                    <th>Удалить</th>
                </tr>
                <?php foreach ($registrations as $registration) { ?>
                    <tr>
                        <td><?= $registration['name'] ?></td>
                        <td><?= $registration['surname'] ?></td>
                        <td><?= $registration['email'] ?></td>
                        <td><?= $registration['phone'] ?></td>
                        <td><?= $registration['conference'] ?></td>
                        <td><?= $registration['payment_method'] ?></td>
                        <td><?= $registration['newsletter'] ?></td>
                        <td><?= $registration['submission_date'] ?></td>
                        <td><?= $registration['ip_address'] ?></td>
                        <td>
                            <input type="checkbox" name="delete[]" value="<?= implode("|", $registration) ?>">
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <p><input type="submit" value="Удалить заявки" name="done"></p>
        </form>
        <form method="post">
            <input type="submit" value="Выход" name="logout">
        </form>
    <?php } ?>
</body>

</html>
